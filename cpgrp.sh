#!/bin/bash
#
# Copy group membership from one account to another

function yes_no() {
  read -p "$1 ([y]es or [N]o): "
  case $(echo $REPLY | tr '[A-Z]' '[a-z]') in
    y|yes) echo "yes" ;;
    *)     echo "no" ;;
  esac
}

if [[ ! getopts qf:t: opts ]]; then
  echo "Usage: $(basename $0) [-q] -f username1 -t usename2";
  exit 1;
fi

while getopts qf:t: opts; do
  case ${opts} in
    q) quiet=1 ;;
    f) from_usr=${OPTARG} ;;
    t) to_usr=${OPTARG} ;;
    \?) echo "Invalid option: -$OPTARG" ;;
    :) echo "Option -$OPTARG requires an argument."
      exit 2
    ;;
  esac
done

for grps in $(groups ${from_usr} | cut -f2 -d':') ;do
  if [[ ${quiet} ]]; then
    usermod -A ${grps} ${to_usr} &> /dev/null || echo "Unable to add user ${to_usr} to ${grps};
  elif [[ "yes" == $(yes_no "Add ${to_usr} to group ${grps}?")]]; then
      usermod -A ${grps} ${to_usr} &> /dev/null || echo "Unable to add user ${to_usr} to ${grps}";
  fi
done
